import React, { Component } from 'react'

export default class ItemGlasses extends Component {
  render() {
    return (
      <div className='col-2'>
        <div onClick={()=>{
            this.props.changeDetail(this.props.glasses)
        }}>
            <img className='imgGlasses' src={this.props.glasses.url}alt="" />
        </div>
      </div>
    )
  }
}
