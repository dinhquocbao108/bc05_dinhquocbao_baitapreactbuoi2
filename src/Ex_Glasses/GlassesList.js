import React, { Component } from "react";
import ItemGlasses from "./ItemGlasses";

export default class GlassesList extends Component {
  renderGlasses = () => {
    return this.props.glassesArr.map((item) => {
      return <ItemGlasses changeDetail={this.props.changeDetail}  glasses={item}/>;
    });
  };
  render() {
    return <div className="row glassesList">{this.renderGlasses()}</div>;
  }
}
