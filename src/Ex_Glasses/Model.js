import React, { Component } from "react";
import DetailGlasses from "./DetailGlasses";

export default class Model extends Component {
  render() {
    return (
      <div className="row my-5">
        <div className="vglasses__model col-5 ">
        <img src={this.props.detail.url} alt="" />
        <DetailGlasses detail={this.props.detail}/>
        </div>
        <div className="vglasses__model  col-5 ml-auto ">
          <img src={this.props.detail.url} alt="" />
        </div>
      </div>
    );
  }
}
