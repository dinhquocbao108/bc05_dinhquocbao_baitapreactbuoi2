import React, { Component } from 'react'

export default class DetailGlasses extends Component {
  render() {
    return (
      <div className='vglasses__info'>
        <p>{this.props.detail.price}"$"</p>
        <p>{this.props.detail.name}</p>
        <p>{this.props.detail.desc}</p>
      </div>
    )
  }
}
