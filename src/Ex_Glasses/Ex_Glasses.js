import React, { Component } from "react";
import GlassesList from "./GlassesList";
import Header from "./Header";
import Model from "./Model";
import { dataGlasses } from "./dataGlasses";

export default class extends Component {
  state = {
    glassesArr: dataGlasses,
    detail: dataGlasses[0],
  };
  changeDetail = (glasses) => {
    this.setState({ detail: glasses });
  };
  render() {
    return (
      <div>
        <div className="total">
          <Header />
          <div className="container">
            <Model detail={this.state.detail} />
            <GlassesList 
            changeDetail={this.changeDetail}
            glassesArr={this.state.glassesArr} />
          </div>
        </div>
      </div>
    );
  }
}
